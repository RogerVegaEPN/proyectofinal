//
//  RegisterViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 19/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegisterViewController: UIViewController {
    
    var concierto : conciertoStruct!
    var flag = ""
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        revisarUsuarioLogueado()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func registrarseButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if (error != nil){
                self.errorMessageLabel.text = error?.localizedDescription as? String
            }
            self.revisarUsuarioLogueado()
            
        }
    }
    
    @IBAction func loginButton(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if (error != nil){
                self.errorMessageLabel.text = error?.localizedDescription as? String
            }
            self.revisarUsuarioLogueado()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueComprar"){
            let destination = segue.destination as! ComprarViewController
            destination.concierto = concierto
        }
    }
    
    func revisarUsuarioLogueado(){
        if Auth.auth().currentUser != nil {
            performSegue(withIdentifier: "segueComprar", sender: self)
        }
    }
    
    
}
