//
//  ViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 16/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

struct conciertoStruct{
    let title : String!
    let fecha : String!
    let descripcion: String!
    let imagen: String!
}

class TableViewController: UITableViewController {
    var nombreConcierto : conciertoStruct!
    var conciertos = [conciertoStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref.child("concierto").observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let llaves = value?.allKeys
            for i in llaves!{
                print (i as! String)
                ref.child("concierto").child(i as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let title = value?["nombre"] as? String ?? ""
                    let fecha = value?["fecha"] as? String ?? ""
                    let descripcion = value?["descripcion"] as? String ?? ""
                    let imagen = value?["imagen"] as? String ?? ""
                    self.conciertos.insert(conciertoStruct(title: title, fecha: fecha, descripcion: descripcion, imagen: imagen), at: 0)
                    self.tableView.reloadData()
                }) { (error) in
                    print(error.localizedDescription)
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conciertos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        let label1 = cell?.viewWithTag(1) as! UILabel
        label1.text = conciertos[indexPath.row].title
        
        let label2 = cell?.viewWithTag(2) as! UILabel
        label2.text = conciertos[indexPath.row].fecha
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nombreConcierto = conciertos[indexPath.row]
        self.performSegue(withIdentifier: "segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue"){
            let destination = segue.destination as! DetalleConciertoViewController
            destination.concierto = nombreConcierto
        }
        
    }
}
