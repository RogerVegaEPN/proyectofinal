//
//  ComprarViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 19/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
class ComprarViewController: UIViewController {
    
    
    @IBOutlet weak var asientosText: UILabel!
    @IBOutlet weak var valorPagar: UILabel!
    
    
    var concierto : conciertoStruct!
    var arrayAsientos = [String](){
        didSet {
            updateLabel()
        }
    }
    

    
    @IBOutlet weak var A1: UIButton!
    @IBOutlet weak var B1: UIButton!
    @IBOutlet weak var C1: UIButton!
    @IBOutlet weak var D1: UIButton!
    @IBOutlet weak var E1: UIButton!
    @IBOutlet weak var F1: UIButton!
    @IBOutlet weak var G1: UIButton!
    @IBOutlet weak var H1: UIButton!
    @IBOutlet weak var A2: UIButton!
    @IBOutlet weak var B2: UIButton!
    @IBOutlet weak var C2: UIButton!
    @IBOutlet weak var D2: UIButton!
    @IBOutlet weak var E2: UIButton!
    @IBOutlet weak var F2: UIButton!
    @IBOutlet weak var G2: UIButton!
    @IBOutlet weak var H2: UIButton!
    @IBOutlet weak var A3: UIButton!
    @IBOutlet weak var B3: UIButton!
    @IBOutlet weak var C3: UIButton!
    @IBOutlet weak var D3: UIButton!
    @IBOutlet weak var E3: UIButton!
    @IBOutlet weak var F3: UIButton!
    @IBOutlet weak var G3: UIButton!
    @IBOutlet weak var H3: UIButton!
    @IBOutlet weak var A4: UIButton!
    @IBOutlet weak var B4: UIButton!
    @IBOutlet weak var C4: UIButton!
    @IBOutlet weak var D4: UIButton!
    @IBOutlet weak var E4: UIButton!
    @IBOutlet weak var F4: UIButton!
    @IBOutlet weak var G4: UIButton!
    @IBOutlet weak var H4: UIButton!
    @IBOutlet weak var A5: UIButton!
    @IBOutlet weak var B5: UIButton!
    @IBOutlet weak var C5: UIButton!
    @IBOutlet weak var D5: UIButton!
    @IBOutlet weak var E5: UIButton!
    @IBOutlet weak var F5: UIButton!
    @IBOutlet weak var G5: UIButton!
    @IBOutlet weak var H5: UIButton!
    @IBOutlet weak var A6: UIButton!
    @IBOutlet weak var B6: UIButton!
    @IBOutlet weak var C6: UIButton!
    @IBOutlet weak var D6: UIButton!
    @IBOutlet weak var E6: UIButton!
    @IBOutlet weak var F6: UIButton!
    @IBOutlet weak var G6: UIButton!
    @IBOutlet weak var H6: UIButton!
    @IBOutlet weak var A7: UIButton!
    @IBOutlet weak var B7: UIButton!
    @IBOutlet weak var C7: UIButton!
    @IBOutlet weak var D7: UIButton!
    @IBOutlet weak var E7: UIButton!
    @IBOutlet weak var F7: UIButton!
    @IBOutlet weak var G7: UIButton!
    @IBOutlet weak var H7: UIButton!
    @IBOutlet weak var A8: UIButton!
    @IBOutlet weak var B8: UIButton!
    @IBOutlet weak var C8: UIButton!
    @IBOutlet weak var D8: UIButton!
    @IBOutlet weak var E8: UIButton!
    @IBOutlet weak var F8: UIButton!
    @IBOutlet weak var G8: UIButton!
    @IBOutlet weak var H8: UIButton!
 
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = concierto.title
        asientosText.text = ""
        getUsuario()
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref.child("ticketsComprados").child(concierto.title).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let llaves = value?.allKeys
            if value != nil{
                for i in llaves!{
                    print (i as! String)
                    self.desabilitarBotton(asiento: i as! String)
                }
            }

        }) { (error) in
            print(error.localizedDescription)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func A1(_ sender: Any) {
        A1.backgroundColor = .orange
        arrayAsientos.append("A1")
        A1.isEnabled = false
    }
    @IBAction func A2(_ sender: Any) {
        A2.backgroundColor = .orange
        arrayAsientos.append("A2")
        A2.isEnabled = false
    }
    @IBAction func A3(_ sender: Any) {
        A3.backgroundColor = .orange
        arrayAsientos.append("A3")
        A3.isEnabled = false
    }
    @IBAction func A4(_ sender: Any) {
        A4.backgroundColor = .orange
        arrayAsientos.append("A4")
        A4.isEnabled = false
    }
    @IBAction func A5(_ sender: Any) {
        A5.backgroundColor = .orange
        arrayAsientos.append("A5")
        A5.isEnabled = false
    }
    @IBAction func A6(_ sender: Any) {
        A6.backgroundColor = .orange
        arrayAsientos.append("A6")
        A6.isEnabled = false
    }
    @IBAction func A7(_ sender: Any) {
        A7.backgroundColor = .orange
        arrayAsientos.append("A7")
        A7.isEnabled = false
    }
    @IBAction func A8(_ sender: Any) {
        A8.backgroundColor = .orange
        arrayAsientos.append("A8")
        A8.isEnabled = false
    }
    
    @IBAction func B1(_ sender: Any) {
        B1.backgroundColor = .orange
        arrayAsientos.append("B1")
        B1.isEnabled = false
    }
    @IBAction func B2(_ sender: Any) {
        B2.backgroundColor = .orange
        arrayAsientos.append("B2")
        B2.isEnabled = false
    }
    @IBAction func B3(_ sender: Any) {
        B3.backgroundColor = .orange
        arrayAsientos.append("B3")
        B3.isEnabled = false
    }
    @IBAction func B4(_ sender: Any) {
        B4.backgroundColor = .orange
        arrayAsientos.append("B4")
        B4.isEnabled = false
    }
    @IBAction func B5(_ sender: Any) {
        B5.backgroundColor = .orange
        arrayAsientos.append("B5")
        B5.isEnabled = false
    }
    @IBAction func B6(_ sender: Any) {
        B6.backgroundColor = .orange
        arrayAsientos.append("B6")
        B6.isEnabled = false
    }
    @IBAction func B7(_ sender: Any) {
        B7.backgroundColor = .orange
        arrayAsientos.append("B7")
        B7.isEnabled = false
    }
    @IBAction func B8(_ sender: Any) {
        B8.backgroundColor = .orange
        arrayAsientos.append("B8")
        B8.isEnabled = false
    }
    
    @IBAction func C1(_ sender: Any) {
        C1.backgroundColor = .orange
        arrayAsientos.append("C1")
        C1.isEnabled = false
    }
    @IBAction func C2(_ sender: Any) {
        C2.backgroundColor = .orange
        arrayAsientos.append("C2")
        C2.isEnabled = false
    }
    @IBAction func C3(_ sender: Any) {
        C3.backgroundColor = .orange
        arrayAsientos.append("C3")
        C3.isEnabled = false
    }
    @IBAction func C4(_ sender: Any) {
        C4.backgroundColor = .orange
        arrayAsientos.append("C4")
        C4.isEnabled = false
    }
    @IBAction func C5(_ sender: Any) {
        C5.backgroundColor = .orange
        arrayAsientos.append("C5")
        C5.isEnabled = false
    }
    @IBAction func C6(_ sender: Any) {
        C6.backgroundColor = .orange
        arrayAsientos.append("C6")
        C6.isEnabled = false
    }
    @IBAction func C7(_ sender: Any) {
        C7.backgroundColor = .orange
        arrayAsientos.append("C7")
        C7.isEnabled = false
    }
    @IBAction func C8(_ sender: Any) {
        C8.backgroundColor = .orange
        arrayAsientos.append("C8")
        C8.isEnabled = false
    }
    
    @IBAction func D1(_ sender: Any) {
        D1.backgroundColor = .orange
        arrayAsientos.append("D1")
        D1.isEnabled = false
    }
    @IBAction func D2(_ sender: Any) {
        D2.backgroundColor = .orange
        arrayAsientos.append("D2")
        D2.isEnabled = false
    }
    @IBAction func D3(_ sender: Any) {
        D3.backgroundColor = .orange
        arrayAsientos.append("D3")
        D3.isEnabled = false
    }
    @IBAction func D4(_ sender: Any) {
        D4.backgroundColor = .orange
        arrayAsientos.append("D4")
        D4.isEnabled = false
    }
    @IBAction func D5(_ sender: Any) {
        D5.backgroundColor = .orange
        arrayAsientos.append("D5")
        D5.isEnabled = false
    }
    @IBAction func D6(_ sender: Any) {
        D6.backgroundColor = .orange
        arrayAsientos.append("D6")
        D6.isEnabled = false
    }
    @IBAction func D7(_ sender: Any) {
        D7.backgroundColor = .orange
        arrayAsientos.append("D7")
        D7.isEnabled = false
    }
    @IBAction func D8(_ sender: Any) {
        D8.backgroundColor = .orange
        arrayAsientos.append("D8")
        D8.isEnabled = false
    }
    
    
    @IBAction func E1(_ sender: Any) {
        E1.backgroundColor = .orange
        arrayAsientos.append("E1")
        E1.isEnabled = false
    }
    @IBAction func E2(_ sender: Any) {
        E2.backgroundColor = .orange
        arrayAsientos.append("E2")
        E2.isEnabled = false
    }
    @IBAction func E3(_ sender: Any) {
        E3.backgroundColor = .orange
        arrayAsientos.append("E3")
        E3.isEnabled = false
    }
    @IBAction func E4(_ sender: Any) {
        E4.backgroundColor = .orange
        arrayAsientos.append("E4")
        E4.isEnabled = false
    }
    @IBAction func E5(_ sender: Any) {
        E5.backgroundColor = .orange
        arrayAsientos.append("E5")
        E5.isEnabled = false
    }
    @IBAction func E6(_ sender: Any) {
        E6.backgroundColor = .orange
        arrayAsientos.append("E6")
        E6.isEnabled = false
    }
    @IBAction func E7(_ sender: Any) {
        E7.backgroundColor = .orange
        arrayAsientos.append("E7")
        E7.isEnabled = false
    }
    @IBAction func E8(_ sender: Any) {
        E8.backgroundColor = .orange
        arrayAsientos.append("E8")
        E8.isEnabled = false
    }
    
    
    @IBAction func F1(_ sender: Any) {
        F1.backgroundColor = .orange
        arrayAsientos.append("F1")
        F1.isEnabled = false
    }
    @IBAction func F2(_ sender: Any) {
        F2.backgroundColor = .orange
        arrayAsientos.append("F2")
        F2.isEnabled = false
    }
    @IBAction func F3(_ sender: Any) {
        F3.backgroundColor = .orange
        arrayAsientos.append("F3")
        F3.isEnabled = false
    }
    @IBAction func F4(_ sender: Any) {
        F4.backgroundColor = .orange
        arrayAsientos.append("F4")
        F4.isEnabled = false
    }
    @IBAction func F5(_ sender: Any) {
        F5.backgroundColor = .orange
        arrayAsientos.append("F5")
        F5.isEnabled = false
    }
    @IBAction func F6(_ sender: Any) {
        F6.backgroundColor = .orange
        arrayAsientos.append("F6")
        F6.isEnabled = false
    }
    @IBAction func F7(_ sender: Any) {
        F7.backgroundColor = .orange
        arrayAsientos.append("F7")
        F7.isEnabled = false
    }
    @IBAction func F8(_ sender: Any) {
        F8.backgroundColor = .orange
        arrayAsientos.append("F8")
        F8.isEnabled = false
    }
    
    @IBAction func G1(_ sender: Any) {
        G1.backgroundColor = .orange
        arrayAsientos.append("G1")
        G1.isEnabled = false
    }
    @IBAction func G2(_ sender: Any) {
        G2.backgroundColor = .orange
        arrayAsientos.append("G2")
        G2.isEnabled = false
    }
    @IBAction func G3(_ sender: Any) {
        G3.backgroundColor = .orange
        arrayAsientos.append("G3")
        G3.isEnabled = false
    }
    @IBAction func G4(_ sender: Any) {
        G4.backgroundColor = .orange
        arrayAsientos.append("G4")
        G4.isEnabled = false
    }
    @IBAction func G5(_ sender: Any) {
        G5.backgroundColor = .orange
        arrayAsientos.append("G5")
        G5.isEnabled = false
    }
    @IBAction func G6(_ sender: Any) {
        G6.backgroundColor = .orange
        arrayAsientos.append("G6")
        G6.isEnabled = false
    }
    @IBAction func G7(_ sender: Any) {
        G7.backgroundColor = .orange
        arrayAsientos.append("G7")
        G7.isEnabled = false
    }
    @IBAction func G8(_ sender: Any) {
        G8.backgroundColor = .orange
        arrayAsientos.append("G8")
        G8.isEnabled = false
    }
    
    @IBAction func H1(_ sender: Any) {
        H1.backgroundColor = .orange
        arrayAsientos.append("H1")
        H1.isEnabled = false
    }
    @IBAction func H2(_ sender: Any) {
        H2.backgroundColor = .orange
        arrayAsientos.append("H2")
        H2.isEnabled = false
    }
    @IBAction func H3(_ sender: Any) {
        H3.backgroundColor = .orange
        arrayAsientos.append("H3")
        H3.isEnabled = false
    }
    @IBAction func H4(_ sender: Any) {
        H4.backgroundColor = .orange
        arrayAsientos.append("H4")
        H4.isEnabled = false
    }
    @IBAction func H5(_ sender: Any) {
        H5.backgroundColor = .orange
        arrayAsientos.append("H5")
        H5.isEnabled = false
    }
    @IBAction func H6(_ sender: Any) {
        H6.backgroundColor = .orange
        arrayAsientos.append("H6")
        H6.isEnabled = false
    }
    @IBAction func H7(_ sender: Any) {
        H7.backgroundColor = .orange
        arrayAsientos.append("H7")
        H7.isEnabled = false
    }
    @IBAction func H8(_ sender: Any) {
        H8.backgroundColor = .orange
        arrayAsientos.append("H8")
        H8.isEnabled = false
    }
    
    
    func desabilitarBotton(asiento:String){
        switch asiento {
        case "A1":
            A1.isEnabled = false
            A1.backgroundColor = .red
        case "A2":
            A2.isEnabled = false
            A2.backgroundColor = .red
        case "A3":
            A3.isEnabled = false
            A3.backgroundColor = .red
        case "A4":
            A4.isEnabled = false
            A4.backgroundColor = .red
        case "A5":
            A5.isEnabled = false
            A5.backgroundColor = .red
        case "A6":
            A6.isEnabled = false
            A6.backgroundColor = .red
        case "A7":
            A7.isEnabled = false
            A7.backgroundColor = .red
        case "A8":
            A8.isEnabled = false
            A8.backgroundColor = .red
        
        case "B1":
            B1.isEnabled = false
            B1.backgroundColor = .red
        case "B2":
            B2.isEnabled = false
            B2.backgroundColor = .red
        case "B3":
            B3.isEnabled = false
            B3.backgroundColor = .red
        case "B4":
            B4.isEnabled = false
            B4.backgroundColor = .red
        case "B5":
            B5.isEnabled = false
            B5.backgroundColor = .red
        case "B6":
            B6.isEnabled = false
            B6.backgroundColor = .red
        case "B7":
            B7.isEnabled = false
            B7.backgroundColor = .red
        case "B8":
            B8.isEnabled = false
            B8.backgroundColor = .red
            
        case "C1":
            C1.isEnabled = false
            C1.backgroundColor = .red
        case "C2":
            C2.isEnabled = false
            C2.backgroundColor = .red
        case "C3":
            C3.isEnabled = false
            C3.backgroundColor = .red
        case "C4":
            C4.isEnabled = false
            C4.backgroundColor = .red
        case "C5":
            C5.isEnabled = false
            C5.backgroundColor = .red
        case "C6":
            C6.isEnabled = false
            C6.backgroundColor = .red
        case "C7":
            C7.isEnabled = false
            C7.backgroundColor = .red
        case "C8":
            C8.isEnabled = false
            C8.backgroundColor = .red
            
        case "D1":
            D1.isEnabled = false
            D1.backgroundColor = .red
        case "D2":
            D2.isEnabled = false
            D2.backgroundColor = .red
        case "D3":
            D3.isEnabled = false
            D3.backgroundColor = .red
        case "D4":
            D4.isEnabled = false
            D4.backgroundColor = .red
        case "D5":
            D5.isEnabled = false
            D5.backgroundColor = .red
        case "D6":
            D6.isEnabled = false
            D6.backgroundColor = .red
        case "D7":
            D7.isEnabled = false
            D7.backgroundColor = .red
        case "D8":
            D8.isEnabled = false
            D8.backgroundColor = .red
            
        case "E1":
            E1.isEnabled = false
            E1.backgroundColor = .red
        case "E2":
            E2.isEnabled = false
            E2.backgroundColor = .red
        case "E3":
            E3.isEnabled = false
            E3.backgroundColor = .red
        case "E4":
            E4.isEnabled = false
            E4.backgroundColor = .red
        case "E5":
            E5.isEnabled = false
            E5.backgroundColor = .red
        case "E6":
            E6.isEnabled = false
            E6.backgroundColor = .red
        case "E7":
            E7.isEnabled = false
            E7.backgroundColor = .red
        case "E8":
            E8.isEnabled = false
            E8.backgroundColor = .red
            
        case "F1":
            F1.isEnabled = false
            F1.backgroundColor = .red
        case "F2":
            F2.isEnabled = false
            F2.backgroundColor = .red
        case "F3":
            F3.isEnabled = false
            F3.backgroundColor = .red
        case "F4":
            F4.isEnabled = false
            F4.backgroundColor = .red
        case "F5":
            F5.isEnabled = false
            F5.backgroundColor = .red
        case "F6":
            F6.isEnabled = false
            F6.backgroundColor = .red
        case "F7":
            F7.isEnabled = false
            F7.backgroundColor = .red
        case "F8":
            F8.isEnabled = false
            F8.backgroundColor = .red
        
        case "G1":
            G1.isEnabled = false
            G1.backgroundColor = .red
        case "G2":
            G2.isEnabled = false
            G2.backgroundColor = .red
        case "G3":
            G3.isEnabled = false
            G3.backgroundColor = .red
        case "G4":
            G4.isEnabled = false
            G4.backgroundColor = .red
        case "F5":
            G5.isEnabled = false
            G5.backgroundColor = .red
        case "G6":
            G6.isEnabled = false
            G6.backgroundColor = .red
        case "G7":
            G7.isEnabled = false
            G7.backgroundColor = .red
        case "G8":
            G8.isEnabled = false
            G8.backgroundColor = .red
            
        case "H1":
            H1.isEnabled = false
            H1.backgroundColor = .red
        case "H2":
            H2.isEnabled = false
            H2.backgroundColor = .red
        case "H3":
            H3.isEnabled = false
            H3.backgroundColor = .red
        case "H4":
            H4.isEnabled = false
            H4.backgroundColor = .red
        case "H5":
            H5.isEnabled = false
            H5.backgroundColor = .red
        case "H6":
            H6.isEnabled = false
            H6.backgroundColor = .red
        case "H7":
            H7.isEnabled = false
            H7.backgroundColor = .red
        case "H8":
            H8.isEnabled = false
            H8.backgroundColor = .red
            
        default: break
            
        }
    }
    
    func updateLabel(){
        var asientos = ""
        let totalAsientos = arrayAsientos.count
        for a in arrayAsientos{
            print(a)
             asientos += "\(a), "
        }
        asientosText.text = asientos
        valorPagar.text = String(totalAsientos * 25)
    }
    
    
    @IBAction func comprarButton(_ sender: Any) {
        
        if valorPagar.text != "0"{
            performSegue(withIdentifier: "seguePagar", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "seguePagar"){
            let destination = segue.destination as! PagarViewController
            destination.arrayAsientos = arrayAsientos
            destination.concierto = concierto
        }
        if(segue.identifier == "segueLogout"){
            let destination = segue.destination as! InicioViewController
        }
    }
    
    
    @IBAction func logOutButton(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            performSegue(withIdentifier: "segueLogout", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func getUsuario(){
        let user = Auth.auth().currentUser
        if let user = user {
            let uid = user.uid
            let email = user.email
        }
    }

}
