//
//  DetalleConciertoViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 16/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class DetalleConciertoViewController: UIViewController {
    var concierto : conciertoStruct!
    
    @IBOutlet weak var descripcionText: UITextView!
    
    @IBOutlet weak var imagenConcierto: UIImageView!
    
    @IBOutlet weak var comprarTicketsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = concierto.title
        descripcionText.text = concierto.descripcion
        if let url = URL(string: concierto.imagen) {
            imagenConcierto.contentMode = .scaleAspectFit
            downloadImage(url: url)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.imagenConcierto.image = UIImage(data: data)
            }
        }
    }
    
   
    @IBAction func comprarTicketsButton(_ sender: Any) {
        revisarUsuarioLogueado()
    }
    
    func revisarUsuarioLogueado(){
        if Auth.auth().currentUser != nil {
            performSegue(withIdentifier: "segueComprarLogueado", sender: self)
        } else {
            self.performSegue(withIdentifier: "segueLogin", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueLogin"){
            let destination = segue.destination as! RegisterViewController
            destination.concierto = concierto
        }
        if(segue.identifier == "segueComprarLogueado"){
            let destination = segue.destination as! ComprarViewController
            destination.concierto = concierto
        }
    }
    
}
