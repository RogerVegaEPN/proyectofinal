//
//  InicioViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 23/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class InicioViewController: UIViewController {

    
    @IBOutlet weak var welcome: UIImageView!
    let imagenWelcome = "https://ep01.epimg.net/cultura/imagenes/2016/12/01/actualidad/1480576411_932413_1480578681_noticia_normal.jpg"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: imagenWelcome) {
            welcome.contentMode = .scaleAspectFit
            downloadImage(url: url)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            DispatchQueue.main.async() {
                self.welcome.image = UIImage(data: data)
            }
        }
    }
    

}
