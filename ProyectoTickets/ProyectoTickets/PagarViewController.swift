//
//  PagarViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 23/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class PagarViewController: UIViewController {
    
    var arrayAsientos = [String]()
    var concierto : conciertoStruct!
    var mailUsuario = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Pago"
        getUsuario()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func comprarbutton(_ sender: Any) {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        for a in arrayAsientos{
            ref.child("ticketsComprados").child(concierto.title).updateChildValues([a: mailUsuario])
        }
        performSegue(withIdentifier: "seguePagado", sender: self)
    }
    
    func getUsuario(){
        let user = Auth.auth().currentUser
        if let user = user {
            mailUsuario = user.email!
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "seguePagado"){
            let destination = segue.destination as! InicioViewController
        }
    }
}
