//
//  LoginViewController.swift
//  ProyectoTickets
//
//  Created by Roger on 19/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
        
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
    }
    
  
    @IBAction func registrarseButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            print(user ?? "No hay usuario")
            print(error ?? "No hay error")
        }
    }
 
}
